#!/bin/bash

set -e
set -x

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=$script_dir/../src

mkdir -p $dist_dir

rsync -aH $src_dir/ $dist_dir

png_files=$(cd ${dist_dir} && find . -type f -name '*.png')

IFS=$'\n'
for png_file in $png_files
do
  basename=$(basename -- "${png_file}")
  dirname=$(dirname -- "${png_file}")
  filename="${basename%.*}"

  png=${dist_dir}/${png_file}
  png_prefix=${dist_dir}/${dirname}/${filename}

  # jpg
  convert ${png} ${png_prefix}.jpg

  for size in 800 600 450 250 150 100 45
  do
    convert ${png} -resize ${size}x${size} ${png_prefix}-${size}.png
    convert ${png_prefix}.jpg -resize ${size}x${size} ${png_prefix}-${size}.jpg
  done
done
IFS="$OIFS"
